#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    client = new QNetworkAccessManager(this);
    pause = true;
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_startButton_clicked()
{
    if (pause) {
        pause = false;
        loadImage();
        ui->startButton->setText("Pause");
    }
    else {
        pause = true;
        ui->startButton->setText("Start");
    }
}

void Widget::loadImage()
{
    QUrl remote(ui->lineEdit->text());
    QNetworkRequest request(remote);
    reply = client->get(request);
    connect(reply, SIGNAL(finished()), this, SLOT(updateImage()));
}

void Widget::updateImage()
{
    QPixmap image;
    image.loadFromData(reply->readAll(), "JPEG");
    ui->label->setPixmap(image);
    //信号处理完成后，需要释放reply对象的内存
    reply->deleteLater();
    if (!pause) {
        loadImage();
    }
}
