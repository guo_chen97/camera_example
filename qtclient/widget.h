#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QNetworkAccessManager>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    void loadImage();

private slots:
    void on_startButton_clicked();
    void updateImage();

private:
    Ui::Widget *ui;
    QNetworkAccessManager* client;
    QNetworkReply* reply;
    bool pause;
};
#endif // WIDGET_H
