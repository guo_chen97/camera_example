//获取当前摄像头驱动支持的图像格式
//功能类似以下命令
// v4l2-ctl -d /dev/video0 --list-formats-ext

#include <stdio.h> //perror
//open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h> //close
#include <sys/ioctl.h> //ioctl
#include <stdlib.h> //EXIT_FAILURE
#include <linux/videodev2.h> //V4L2
#include <string.h> //memset
#include <sys/mman.h> //mmap

int main()
{
    //打开设备文件，需要读写权限
    int fd = open("/dev/video0", O_RDWR);
    if (fd < 0)
    {
        perror("open");
        return EXIT_FAILURE;
    }

    struct v4l2_fmtdesc fmtdesc;
    fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmtdesc.index = 0;
    puts("support formats:");
    while (!ioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc))
    {
        printf("%s\n", fmtdesc.description);
        //获取当前格式支持的分辨率
        struct v4l2_frmsizeenum frmsize;
        frmsize.pixel_format = fmtdesc.pixelformat;
        frmsize.index = 0;
        while (!ioctl(fd, VIDIOC_ENUM_FRAMESIZES, &frmsize))
        {
            if (V4L2_FRMSIZE_TYPE_DISCRETE == frmsize.type)
            {
                printf("  %d x %d\n", frmsize.discrete.width, frmsize.discrete.height);
                frmsize.index++;
            }
            else
            {
                break;
            }
        }
        fmtdesc.index++;
    }

    close(fd);
    return EXIT_SUCCESS;
}
